---

marp: true

---


![bg right:50% brightness:. sepia:50%](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/鹿头002.png)




#### [${\color{blue}{连享会·直播课}}$](http://lianxh.duanshu.com)

${\color{white}{a}}$

# 五分钟 Markdown

${\color{white}{a}}$

> ### 连玉君 ( [lianxh.cn](https://www.lianxh.cn) )

${\color{white}{a}}$

> ### 项目主页：
> https://gitee.com/arlionn/md
> 资料、幻灯片 Markdown 原文


--- - --

## 🍏 我的第一份 Markdown 文稿

${\color{white}{a}}$
${\color{white}{a}}$



--- - --

![width:1500](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会_五分钟Markdown_V3.png)

--- - --

## 🍎 来个复杂点的

--- - --


## 描述性分析

$\quad
y_{it} - y_{it-1} = \lambda(y^*_{it}-y_{it-1}) \qquad {\color{blue}{0<\lambda<1}}
$

$\quad
y_{it} - y_{it-1} = \phi_i (y_{it-1}-y_{it-2}) \quad {\color{red}{\phi_i<0}}\ ?
$

- Opler, T., L. Pinkowitz, R. Stulz, R. Williamson, **1999**, The determinants and implications of corporate cash holdings, **JFE**, 52 (1): 3-46. [[PDF]](https://quqi.gblhgk.com/s/880197/Wi6cdB3bgYGyKcgV), Figure 3 (pp.19)

```
. regress D.BDR L.D.BDR
. regfit, f(%4.3f)

D.BDR = 0.017 - 0.062*LD.BDR
S.E. (0.001) (0.009)
N = 11644, R2 = 0.004, adj-R2 = 0.004
```

--- - --

```
## 描述性分析

$\quad
y_{it} - y_{it-1} = \lambda(y^*_{it}-y_{it-1}) \qquad {\color{blue}{0<\lambda<1}}
$

$\quad
y_{it} - y_{it-1} = \phi_i (y_{it-1}-y_{it-2}) \quad {\color{red}{\phi_i<0}}\ ?
$

- Opler, T., L. Pinkowitz, R. Stulz, R. Williamson, **1999**,
The determinants and implications of corporate cash holdings,
**JFE**, 52 (1): 3-46. [[PDF]](https://quqi.gblhgk.com/s/880197/Wi6cdB3bgYGyKcgV), Figure 3 (pp.19)


```stata
. regress D.BDR L.D.BDR
. regfit, f(%4.3f)

D.BDR = 0.017 - 0.062*LD.BDR
S.E. (0.001) (0.009)
N = 11644, R2 = 0.004, adj-R2 = 0.004

`` ` (这里多了一个空格)
```

${\color{white}{a}}$

--- - --




## 何时用 Markdown ？

- 记笔记、论文讨论
- 写 [推文](https://www.lianxh.cn)
- 写电子书
- 写 [幻灯片](https://gitee.com/arlionn/md)，e.g. [动态面板](https://quqi.gblhgk.com/s/880197/YKJF278pxgldHQkA) | [我的甲壳虫](https://quqi.gblhgk.com/s/880197/cMEeDSovRZzqAA7s)

## 何时不用 Markdown ？

- 投稿
- ……

--- - --

## 基本语法和链接

- [Markdown 在线教程](https://www.runoob.com/markdown/md-tutorial.html)
- [mdnice.com](https://mdnice.com/)，在线编辑 → 输出 PDF 文档，或贴入微信公众号编辑器，或贴入知乎，或用来将网页转为 Markdown。

- [Markdown + GitBook：电子书](https://www.cntofu.com/book/34/4.md)

--- - --

## Markdown 编辑器

- [主流 Markdown 编辑器推荐](https://zhuanlan.zhihu.com/p/69210764)
- 我用啥？
- [Typora](https://www.typora.io/) → [使用说明](https://sspai.com/post/54912)
- [VS Code](https://code.visualstudio.com/) → [教程1](https://www.cnblogs.com/clwydjgs/p/10078065.html)；[教程2](https://www.cnblogs.com/qianguyihao/archive/2019/04/18/10732375.html)
- mdnice.com
- [有道云笔记](http://note.youdao.com/)，[简书](https://www.jianshu.com/)



--- - --


## Markdown 转 HTML, Word, PDF, 幻灯片

- **PDF**：多数编辑器都支持
- mdnice.com ：「文件」→ 「导出 PDF」 (在线)
- [Typora](https://www.typora.io/) ：「文件」→ 「导出」→ 十多种格式可供选择
- **Word**
- Typora + Pandoc ：「文件」→ 「导出」→ Word   [教程](https://www.jianshu.com/p/79fcb743ae09)
- **幻灯片**
- [web.marp.app](https://web.marp.app/)   [进阶1](https://sspai.com/post/55718)；[进阶2](https://marpit.marp.app/image-syntax?id=image-filters)

--- - --

 

> Stata连享会   [计量专题](https://www.lianxh.cn/news/46917f1076104.html) || [直播视频](http://lianxh.duanshu.com) || [知乎推文](https://www.zhihu.com/people/arlionn/)

![](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。

--- - --

> **资料获取：**
>
> 关注连享会公众号，回复关键词「**Markdown**」或「**五分钟**」获取下载链接。

![欢迎加入Stata连享会(公众号: StataChina)](https://file.lianxh.cn/images/20191111/ec83ed2baf9c93494e4f71c9b0f5d766.png)


--- - --

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-百度一下-主页-码云.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20200319094426.png)